### Hi there 👋 <img src="https://api.dicebear.com/6.x/icons/svg?seed=thisiscoding1234&scale=120&radius=50&backgroundColor=ffcc80,90caf9&backgroundType=gradientLinear&backgroundRotation=360,-360" alt="avatar" style="height:5%;width:5%;" />

<picture>
  <source media="(prefers-color-scheme: dark)" srcset="https://github-readme-stats.vercel.app/api?username=thisiscoding1234&theme=solarized-dark&show_icons=true&show=reviews,discussions_started,discussions_answered,prs_merged,prs_merged_percentage">
  <source media="(prefers-color-scheme: light)" srcset="https://github-readme-stats.vercel.app/api?username=thisiscoding1234&theme=solarized-light&show_icons=true&show=reviews,discussions_started,discussions_answered,prs_merged,prs_merged_percentage">
  <img alt="GitHub statistics!" src="https://github-readme-stats.vercel.app/api?username=thisiscoding1234&theme=transparent&show_icons=true&show=reviews,discussions_started,discussions_answered,prs_merged,prs_merged_percentage">
</picture>

<br/>


<picture>
  <source media="(prefers-color-scheme: dark)" srcset="https://github-readme-streak-stats.herokuapp.com?user=thisiscoding1234&theme=solarized-dark&hide_border=true">
  <source media="(prefers-color-scheme: light)" srcset="https://github-readme-streak-stats.herokuapp.com?user=thisiscoding1234&theme=solarized-light&hide_border=true">
  <img alt="GitHub streak!" src="https://github-readme-streak-stats.herokuapp.com?user=thisiscoding1234&transparent&hide_border=true">
</picture>
